'''This python script basically fetches the required or needed data from the 
AS400 database of Dayton Rogers and publishes it onto Dayton's Server on a web browser.
It's an automated process hence eliminates manual inputting of data or whatsoever respectively'''

#import the requisite python modules needed to create this script
#import ibm_db_dbi as db2
import argparse
import sys
import pyodbc as db2
import logging
import flask
import dash
import dash_html_components as html
import dash_core_components as dcc
from datetime import datetime
from datetime import date
from datetime import time
from datetime import timedelta

def mkdate(datestr):
    try:
        return datetime.strptime(datestr, '%Y-%m-%d')
    except ValueError:
        raise argparse.ArgumentTypeError(datestr + ' is not a proper date string')

    parser=argparse.ArgumentParser()
    parser.add_argument('xDate',type=mkdate)
    args=parser.parse_args()
    return args.xDate

# Set devmode flag for running WSGI dev server or as Gunicorn python app
# Set to True when running Flask test server and running app via python3 command
# Set to False when running app via production Gunicorn server
devmode=True  

# Set up flask server if not in dev mode
if devmode == False:
   server = flask.Flask(__name__) # define flask app.server

log_file = "./logs/" + str(datetime.now().strftime("%Y%m%d_%H%M%S")) + ".log"
logging.basicConfig(filename=log_file, format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO)

# making connection to IBMi database
conn = db2.connect(driver = '{IBM i Access ODBC Driver}', 
                   system = 'dayrog.dr.local', 
                   uid = 'xlsrpt', 
                   pwd = 'e0xvd2hcli')

# creating a variable to hold the cursor function to fetch rows of data from the AS400 database.Execution of queries will be fired with this variable,
#The variable name can be anything not necessarily cursor but for easy readability that's why we are using cursor as the variable name.
cursor = conn.cursor()
cursor2 = conn.cursor()

# Define necessary dates
dates = {}
#dates["todays_date"] = datetime.today() if not devmode else mkdate('2020-07-14')
dates["todays_date"] = datetime.today() if not devmode else mkdate('2020-08-10')

if dates["todays_date"].weekday() == 0:                                                 #If today is Monday
    dates["previous_biz_day"] = dates["todays_date"] - timedelta(days = 3)
    dates["previous_monday"] = dates["todays_date"] - timedelta(days = 7)
else:
    dates["previous_biz_day"] = dates["todays_date"] - timedelta(days = 1)
    dates["previous_monday"] = dates["todays_date"] - timedelta(days = dates["todays_date"].weekday())

if date.today().day == 1:
    dates["first_of_month"] = dates["todays_date"] - timedelta(months = 1)
else:
    dates["first_of_month"] = dates["todays_date"] - timedelta(days=datetime.today().day - 1)

plant = [1, 3, 4, 6]

def mainProgram():
    for i in plant:
        if i==1:
            schema = 'drdata'
            salesName = "drdata.jobrdtpf"
            tableName = "qs36f.ackfileo"
            # quotes
            mnQuoteCount = quoteCount(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            mnMonthlyQuoteCount = quoteCount(schema,dates["first_of_month"],dates["previous_biz_day"])
            
            # orders
            mnReOrders=reOrders(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            mnMonthlyReOrders = reOrders(schema,dates["first_of_month"],dates["previous_biz_day"])
            mnNewDies=newDies(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            mnMonthlyNewDies=newDies(schema,dates["first_of_month"],dates["previous_biz_day"])
            
            # releases
            mnRelToolLabour=relToolLabour(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            mnWeeklyRelToolLabour=relToolLabour(schema,dates["previous_monday"],dates["previous_biz_day"])
            mnRelPressLabour=relPressLabour(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            mnWeeklyRelPressLabour=relPressLabour(schema,dates["previous_monday"],dates["previous_biz_day"])
            
            # shipments
            mn_ship_tool_labor = get_arinv_data('ship_tool_labor', 1, dates["previous_biz_day"], dates["previous_biz_day"])
            mn_ship_tool_labor_week = get_arinv_data('ship_tool_labor', 1, dates["previous_monday"], dates["previous_biz_day"])
            mn_ship_press_labor = get_arinv_data('ship_press_labor', 1, dates["previous_biz_day"], dates["previous_biz_day"])
            mn_ship_press_labor_week = get_arinv_data('ship_press_labor', 1, dates["previous_monday"], dates["previous_biz_day"])
            
            # orders booked
            mnTotalOrders = totalOrdersBooked(schema,dates["previous_biz_day"], dates["previous_biz_day"])
            mnMonthlyTotalOrders = mnTotalOrders
            for days in range(int((dates["previous_biz_day"] - dates["first_of_month"]).days)):
                if dates["first_of_month"].weekday() in range(0,5):
                    mnMonthlyTotalOrders += totalOrdersBooked(schema,dates["first_of_month"] + timedelta(days), dates["first_of_month"] + timedelta(days))
                    logging.info('totalMonthlyTotalOrders schema - %s',schema)
                    logging.info('totalMonthlyTotalOrders Date - %s',dates["first_of_month"] + timedelta(days))
                    logging.info('totalMonthlyTotalOrders Total - %s',mnMonthlyTotalOrders)
            
            # billings
            mn_billings = get_arinv_data('billings', 1, dates["previous_biz_day"],dates["previous_biz_day"])
            mn_billings_month = get_arinv_data('billings', 1, dates["first_of_month"],dates["previous_biz_day"])

            #backlog
            mnSales = salesBackLog(tableName,salesName)
            
        elif i==3:
            schema = 'drohdata'
            salesName = "drohdata.jobrdtpf"
            tableName = "drohdata.ackfileo"
            # quotes
            ohQuoteCount = quoteCount(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            ohMonthlyQuoteCount = quoteCount(schema,dates["first_of_month"],dates["previous_biz_day"])
            
            # orders
            ohReOrders=reOrders(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            ohMonthlyReOrders = reOrders(schema,dates["first_of_month"],dates["previous_biz_day"])
            ohNewDies=newDies(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            ohMonthlyNewDies=newDies(schema,dates["first_of_month"],dates["previous_biz_day"])
            
            # releases
            ohRelToolLabour=relToolLabour(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            ohWeeklyRelToolLabour=relToolLabour(schema,dates["previous_monday"],dates["previous_biz_day"]) 
            ohRelPressLabour=relPressLabour(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            ohWeeklyRelPressLabour=relPressLabour(schema,dates["previous_biz_day"],dates["previous_biz_day"])   
            
            # shipments
            oh_ship_tool_labor = get_arinv_data('ship_tool_labor', 3, dates["previous_biz_day"], dates["previous_biz_day"])
            oh_ship_tool_labor_week = get_arinv_data('ship_tool_labor', 3, dates["previous_monday"], dates["previous_biz_day"])
            oh_ship_press_labor = get_arinv_data('ship_press_labor', 3, dates["previous_biz_day"], dates["previous_biz_day"])
            oh_ship_press_labor_week = get_arinv_data('ship_press_labor', 3, dates["previous_monday"], dates["previous_biz_day"])
            
            # orders booked
            ohTotalOrders = totalOrdersBooked(schema,dates["previous_biz_day"], dates["previous_biz_day"])
            ohMonthlyTotalOrders = ohTotalOrders
            for days in range(int((dates["previous_biz_day"] - dates["first_of_month"]).days)):
                if dates["first_of_month"].weekday() in range(0,5):
                    ohMonthlyTotalOrders += totalOrdersBooked(schema,dates["first_of_month"] + timedelta(days), dates["first_of_month"] + timedelta(days))
                    logging.info('totalMonthlyTotalOrders schema - %s',schema)
                    logging.info('totalMonthlyTotalOrders Date - %s',dates["first_of_month"] + timedelta(days))
                    logging.info('totalMonthlyTotalOrders Total - %s',ohMonthlyTotalOrders)
            
            # billings
            oh_billings = get_arinv_data('billings', 3, dates["previous_biz_day"],dates["previous_biz_day"])
            oh_billings_month = get_arinv_data('billings', 3, dates["first_of_month"],dates["previous_biz_day"])

            #backlog
            ohSales = salesBackLog(tableName,salesName)

        elif i==4:
            schema = 'drtxdata'
            salesName = "drtxdata.jobrdtpf"
            tableName = "drtxdata.ackfileo"
            # quotes
            txQuoteCount = quoteCount(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            txMonthlyQuoteCount = quoteCount(schema,dates["first_of_month"],dates["previous_biz_day"])
            
            # orders
            txReOrders=reOrders(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            txMonthlyReOrders = reOrders(schema,dates["first_of_month"],dates["previous_biz_day"])
            txNewDies=newDies(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            txMonthlyNewDies=newDies(schema,dates["first_of_month"],dates["previous_biz_day"])
            
            # releases
            txRelToolLabour=relToolLabour(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            txWeeklyRelToolLabour=relToolLabour(schema,dates["previous_monday"],dates["previous_biz_day"])
            txRelPressLabour=relPressLabour(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            txWeeklyRelPressLabour=relPressLabour(schema,dates["previous_monday"],dates["previous_biz_day"])
            
            # shipments
            tx_ship_tool_labor = get_arinv_data('ship_tool_labor', 4, dates["previous_biz_day"], dates["previous_biz_day"])
            tx_ship_tool_labor_week = get_arinv_data('ship_tool_labor', 4, dates["previous_monday"], dates["previous_biz_day"])
            tx_ship_press_labor = get_arinv_data('ship_press_labor', 4, dates["previous_biz_day"], dates["previous_biz_day"])
            tx_ship_press_labor_week = get_arinv_data('ship_press_labor', 4, dates["previous_monday"], dates["previous_biz_day"])
            
            # orders booked
            txTotalOrders = totalOrdersBooked(schema,dates["previous_biz_day"], dates["previous_biz_day"])
            txMonthlyTotalOrders = txTotalOrders
            for days in range(int((dates["previous_biz_day"] - dates["first_of_month"]).days)):
                if dates["first_of_month"].weekday() in range(0,5):
                    txMonthlyTotalOrders += totalOrdersBooked(schema,dates["first_of_month"] + timedelta(days), dates["first_of_month"] + timedelta(days))
                    logging.info('totalMonthlyTotalOrders schema - %s',schema)
                    logging.info('totalMonthlyTotalOrders Date - %s',dates["first_of_month"] + timedelta(days))
                    logging.info('totalMonthlyTotalOrders Total - %s',txMonthlyTotalOrders)
            
            # billings
            tx_billings = get_arinv_data('billings', 4, dates["previous_biz_day"],dates["previous_biz_day"])
            tx_billings_month = get_arinv_data('billings', 4, dates["first_of_month"],dates["previous_biz_day"])

            #backlog
            txSales = salesBackLog(tableName,salesName)

        else:
            schema = 'drscdata'
            salesName = "drscdata.jobrdtpf"
            tableName = "drscdata.ackfileo"
            # quotes
            scQuoteCount = quoteCount(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            scMonthlyQuoteCount = quoteCount(schema,dates["first_of_month"],dates["previous_biz_day"])
            
            # orders
            scReOrders=reOrders(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            scMonthlyReOrders = reOrders(schema,dates["first_of_month"],dates["previous_biz_day"])
            scNewDies=newDies(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            scMonthlyNewDies=newDies(schema,dates["first_of_month"],dates["previous_biz_day"])
            
            # releases
            scRelToolLabour=relToolLabour(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            scWeeklyRelToolLabour=relToolLabour(schema,dates["previous_monday"],dates["previous_biz_day"])
            scRelPressLabour=relPressLabour(schema,dates["previous_biz_day"],dates["previous_biz_day"])
            scWeeklyRelPressLabour=relPressLabour(schema,dates["previous_monday"],dates["previous_biz_day"])
            
            # shipments
            sc_ship_tool_labor = get_arinv_data('ship_tool_labor', 6, dates["previous_biz_day"], dates["previous_biz_day"])
            sc_ship_tool_labor_week = get_arinv_data('ship_tool_labor', 6, dates["previous_monday"], dates["previous_biz_day"])
            sc_ship_press_labor = get_arinv_data('ship_press_labor', 6, dates["previous_biz_day"], dates["previous_biz_day"])
            sc_ship_press_labor_week = get_arinv_data('ship_press_labor', 6, dates["previous_monday"], dates["previous_biz_day"])
            
            # orders booked
            scTotalOrders = totalOrdersBooked(schema,dates["previous_biz_day"], dates["previous_biz_day"])
            scMonthlyTotalOrders = scTotalOrders
            for days in range(int((dates["previous_biz_day"] - dates["first_of_month"]).days)):
                if dates["first_of_month"].weekday() in range(0,5):
                    scMonthlyTotalOrders += totalOrdersBooked(schema,dates["first_of_month"] + timedelta(days), dates["first_of_month"] + timedelta(days))
                    logging.info('totalMonthlyTotalOrders schema - %s',schema)
                    logging.info('totalMonthlyTotalOrders Date - %s',dates["first_of_month"] + timedelta(days))
                    logging.info('totalMonthlyTotalOrders Total - %s',scMonthlyTotalOrders)
            
            # billings
            sc_billings = get_arinv_data('billings', 6, dates["previous_biz_day"],dates["previous_biz_day"])
            sc_billings_month = get_arinv_data('billings', 6, dates["first_of_month"],dates["previous_biz_day"])

            #backlog
            scSales = salesBackLog(tableName,salesName)

    
    conn.commit()

    cursor.close()

    conn.close()   

    ####  *****FORECAST NUMBERS****
    mNForecast = 922000
    oHForecast = 775351
    tXForecast = 597667
    sCForecast = 514000
    totalForecast = mNForecast + oHForecast + tXForecast + sCForecast

    # Set up flask app if not in dev mode
    if devmode == True:
       app = dash.Dash() # Dash setup for using Python dev mode
    else:
       app = dash.Dash(__name__, server=server) # call flask server if using Gunicorn
    
    app.layout = html.Div([
        

        html.Div([
            html.Img(src='/assets/daytonLogo.jpg'),
            html.Div([
                html.H1("Dayton Rogers Daily Data"), # Report Heading
                html.H5(dates['previous_biz_day'].strftime('%m/%d/%Y')), # Report Date

            ]),

          html.Div(children=[
            html.Table(children=[  # begins report table
                html.Tr(children=[
                    html.Th(" "),
                    html.Th("Minnesota"),
                    html.Th("Ohio"),
                    html.Th("Texas"),
                    html.Th("South Carolina"),
                    html.Th("Total")
                ]),

                # Quote Count Daily
                html.Tr(children=[
                    html.Th("QUOTATIONS"),
                    html.Td(str(mnQuoteCount)),
                    html.Td(str(ohQuoteCount)),
                    html.Td(str(txQuoteCount)),
                    html.Td(str(scQuoteCount)),
                    html.Td(str(mnQuoteCount + ohQuoteCount + txQuoteCount + scQuoteCount))
                ]),

                # Month Quote Count
                html.Tr(children=[
                    html.Th("QUOTATIONS (Month)"),
                    html.Td(str(mnMonthlyQuoteCount)),
                    html.Td(str(ohMonthlyQuoteCount)),
                    html.Td(str(txMonthlyQuoteCount)),
                    html.Td(str(scMonthlyQuoteCount)),
                    html.Td(str(mnMonthlyQuoteCount + ohMonthlyQuoteCount + txMonthlyQuoteCount + scMonthlyQuoteCount))
                ]),

                # ORDERS - New Dies
                html.Tr(children=[
                    html.Th("ORDERS - New Dies"),
                    html.Td(str(mnNewDies)),
                    html.Td(str(ohNewDies)),
                    html.Td(str(txNewDies)),
                    html.Td(str(scNewDies)),
                    html.Td(str(mnNewDies + ohNewDies + txNewDies + scNewDies))
                ]),

                # ORDERS - New Dies (Month)
                html.Tr(children=[
                    html.Th("New Dies (Month)"),
                    html.Td(str(mnMonthlyNewDies)),
                    html.Td(str(ohMonthlyNewDies)),
                    html.Td(str(txMonthlyNewDies)),
                    html.Td(str(scMonthlyNewDies)),
                    html.Td(str(mnMonthlyNewDies + ohMonthlyNewDies + txMonthlyNewDies + scMonthlyNewDies))
                ]),

                # ORDERS - Reorders
                html.Tr(children=[
                    html.Th("Reorders"),
                    html.Td(str(mnReOrders)),
                    html.Td(str(ohReOrders)),
                    html.Td(str(txReOrders)),
                    html.Td(str(scReOrders)),
                    html.Td(str(mnReOrders + ohReOrders + txReOrders + scReOrders))
                ]),

                # ORDERS - Reorders (Month)
                html.Tr(children=[
                    html.Th("Reorders (Month)"),
                    html.Td(str(mnMonthlyReOrders)),
                    html.Td(str(ohMonthlyReOrders)),
                    html.Td(str(txMonthlyReOrders)),
                    html.Td(str(scMonthlyReOrders)),
                    html.Td(str(mnMonthlyReOrders + ohMonthlyReOrders + txMonthlyReOrders + scMonthlyReOrders))
                ]),

                # RELEASES - Tool Labor
                html.Tr(children=[
                    html.Th("RELEASES - Tool Labor"),
                    html.Td("$"+ '{:,}'.format(mnRelToolLabour)),
                    html.Td("$"+ '{:,}'.format(ohRelToolLabour)),
                    html.Td("$"+ '{:,}'.format(txRelToolLabour)),
                    html.Td("$"+ '{:,}'.format(scRelToolLabour)),
                    html.Td("$"+ '{:,}'.format(mnRelToolLabour + ohRelToolLabour + txRelToolLabour + scRelToolLabour))
                ]),
                # RELEASES - Wk Tool Labor
                html.Tr(children=[
                    html.Th("Tool Labor (Week)"),
                    html.Td("$"+ '{:,}'.format(mnWeeklyRelToolLabour)),
                    html.Td("$"+ '{:,}'.format(ohWeeklyRelToolLabour)),
                    html.Td("$"+ '{:,}'.format(txWeeklyRelToolLabour)),
                    html.Td("$"+ '{:,}'.format(scWeeklyRelToolLabour)),
                    html.Td("$"+ '{:,}'.format(mnWeeklyRelToolLabour + ohWeeklyRelToolLabour + txWeeklyRelToolLabour + scWeeklyRelToolLabour))
                ]),

                # RELEASES - Press Labor
                html.Tr(children=[
                    html.Th("Press Labor"),
                    html.Td("$"+ '{:,}'.format(mnRelPressLabour)),
                    html.Td("$"+ '{:,}'.format(ohRelPressLabour)),
                    html.Td("$"+ '{:,}'.format(txRelPressLabour)),
                    html.Td("$"+ '{:,}'.format(scRelPressLabour)),
                    html.Td("$"+ '{:,}'.format(mnRelPressLabour + ohRelPressLabour + txRelPressLabour + scRelPressLabour))
                ]),

                # RELEASES - Wk Press Labor
                html.Tr(children=[
                    html.Th("Press Labor (Week)"),
                    html.Td("$"+ '{:,}'.format(mnWeeklyRelPressLabour)),
                    html.Td("$"+ '{:,}'.format(ohWeeklyRelPressLabour)),
                    html.Td("$"+ '{:,}'.format(txWeeklyRelPressLabour)),
                    html.Td("$"+ '{:,}'.format(scWeeklyRelPressLabour)),
                    html.Td("$"+ '{:,}'.format(mnWeeklyRelPressLabour + ohWeeklyRelPressLabour + txWeeklyRelPressLabour + scWeeklyRelPressLabour))
                ]),

                # SHIPMENTS - Tool Labor
                html.Tr(children=[
                    html.Th("SHIPMENTS - Tool Labor"),
                    html.Td("$"+ '{:,}'.format(mn_ship_tool_labor)),
                    html.Td("$"+ '{:,}'.format(oh_ship_tool_labor)),
                    html.Td("$"+ '{:,}'.format(tx_ship_tool_labor)),
                    html.Td("$"+ '{:,}'.format(sc_ship_tool_labor)),
                    html.Td("$"+ '{:,}'.format(mn_ship_tool_labor + oh_ship_tool_labor + tx_ship_tool_labor + sc_ship_tool_labor))
                ]),

                # SHIPMENTS- Week Tool Labor
                html.Tr(children=[
                    html.Th("Tool Labor(Week)"),
                    html.Td("$"+ '{:,}'.format(mn_ship_tool_labor_week)),
                    html.Td("$"+ '{:,}'.format(oh_ship_tool_labor_week)),
                    html.Td("$"+ '{:,}'.format(tx_ship_tool_labor_week)),
                    html.Td("$"+ '{:,}'.format(sc_ship_tool_labor_week)),
                    html.Td("$"+ '{:,}'.format(mn_ship_tool_labor_week + oh_ship_tool_labor_week + tx_ship_tool_labor_week + sc_ship_tool_labor_week))
                ]),

                # SHIPMENTS - Press Labor
                html.Tr(children=[
                    html.Th("Press Labor"),
                    html.Td("$"+ '{:,}'.format(mn_ship_press_labor)),
                    html.Td("$"+ '{:,}'.format(oh_ship_press_labor)),
                    html.Td("$"+ '{:,}'.format(tx_ship_press_labor)),
                    html.Td("$"+ '{:,}'.format(sc_ship_press_labor)),
                    html.Td("$"+ '{:,}'.format(mn_ship_press_labor + oh_ship_press_labor + tx_ship_press_labor + sc_ship_press_labor))
                ]),

                # SHIPMENTS - Week Press Labor
                html.Tr(children=[
                    html.Th("Press Labor(Week)"),
                    html.Td("$"+ '{:,}'.format(mn_ship_press_labor_week)),
                    html.Td("$"+ '{:,}'.format(oh_ship_press_labor_week)),
                    html.Td("$"+ '{:,}'.format(tx_ship_press_labor_week)),
                    html.Td("$"+ '{:,}'.format(sc_ship_press_labor_week)),
                    html.Td("$"+ '{:,}'.format(mn_ship_press_labor_week + oh_ship_press_labor_week + tx_ship_press_labor_week + sc_ship_press_labor_week))
                ]),

                # Total Orders Booked
                html.Tr(children=[
                    html.Th("Total Orders Booked"),
                    html.Td("$"+ '{:,}'.format(mnTotalOrders)),
                    html.Td("$"+ '{:,}'.format(ohTotalOrders)),
                    html.Td("$"+ '{:,}'.format(txTotalOrders)),
                    html.Td("$"+ '{:,}'.format(scTotalOrders)),
                    html.Td("$"+ '{:,}'.format(mnTotalOrders + ohTotalOrders + txTotalOrders + scTotalOrders))
                ]),

                # Total Orders Booked (Month)
                html.Tr(children=[
                    html.Th("Total Orders Booked (Month)"),
                    html.Td("$"+ '{:,}'.format(mnMonthlyTotalOrders)),
                    html.Td("$"+ '{:,}'.format(ohMonthlyTotalOrders)),
                    html.Td("$"+ '{:,}'.format(txMonthlyTotalOrders)),
                    html.Td("$"+ '{:,}'.format(scMonthlyTotalOrders)),
                    html.Td("$"+ '{:,}'.format(mnMonthlyTotalOrders + ohMonthlyTotalOrders + txMonthlyTotalOrders + scMonthlyTotalOrders))
                ]),
                
                # Percent Booking/Forecast
                html.Tr(children=[
                    html.Th("% Booking/Forecast"),
                    html.Td('{:,}'.format(round((mnMonthlyTotalOrders/mNForecast)*100)) + "%"),
                    html.Td('{:,}'.format(round((ohMonthlyTotalOrders/oHForecast)*100)) + "%"),
                    html.Td('{:,}'.format(round((txMonthlyTotalOrders/tXForecast)*100)) + "%"),
                    html.Td('{:,}'.format(round((scMonthlyTotalOrders/sCForecast)*100)) + "%"),
                    html.Td('{:,}'.format(round(((mnMonthlyTotalOrders + ohMonthlyTotalOrders + txMonthlyTotalOrders + scMonthlyTotalOrders)/totalForecast)*100)) + "%")
                ]),

                # Billings
                html.Tr(children=[
                    html.Th("Billings (Sales)"),
                    html.Td("$"+ '{:,}'.format(mn_billings)),
                    html.Td("$"+ '{:,}'.format(oh_billings)),
                    html.Td("$"+ '{:,}'.format(tx_billings)),
                    html.Td("$"+ '{:,}'.format(sc_billings)),
                    html.Td("$"+ '{:,}'.format(mn_billings + oh_billings + tx_billings + sc_billings))
                ]),

                # Billings (Month)
                html.Tr(children=[
                    html.Th("Billings/Sales (Month)"),
                    html.Td("$"+ '{:,}'.format(mn_billings_month)),
                    html.Td("$"+ '{:,}'.format(oh_billings_month)),
                    html.Td("$"+ '{:,}'.format(tx_billings_month)),
                    html.Td("$"+ '{:,}'.format(sc_billings_month)),
                    html.Td("$"+ '{:,}'.format(mn_billings_month + oh_billings_month + tx_billings_month + sc_billings_month))
                ]),
            
                # Percent Billings/Forecast 
                html.Tr(children=[
                    html.Th("% Billing/Forecast"),
                    html.Td('{:,}'.format(round((mn_billings_month/mNForecast)*100)) + "%"),
                    html.Td('{:,}'.format(round((oh_billings_month/oHForecast)*100)) + "%"),
                    html.Td('{:,}'.format(round((tx_billings_month/tXForecast)*100)) + "%"),
                    html.Td('{:,}'.format(round((sc_billings_month/sCForecast)*100)) + "%"),
                    html.Td('{:,}'.format(round(((mn_billings_month + oh_billings_month + tx_billings_month + sc_billings_month)/totalForecast)*100)) + "%")
                ]),

                # Forecast Numbers
                html.Tr(children=[
                    html.Th("***FORECAST***"),
                    html.Td("$" + '{:,}'.format(mNForecast)),
                    html.Td("$" + '{:,}'.format(oHForecast)),
                    html.Td("$" + '{:,}'.format(tXForecast)),
                    html.Td("$" + '{:,}'.format(sCForecast)),
                    html.Td("$" + '{:,}'.format(totalForecast)),
                ]),

                # Sales Backlog
                html.Tr(children=[
                    html.Th("Sales Backlog"),
                    html.Td("$" + '{:,}'.format(mnSales)),
                    html.Td("$" + '{:,}'.format(ohSales)),
                    html.Td("$" + '{:,}'.format(txSales)),
                    html.Td("$" + '{:,}'.format(scSales)),
                    html.Td("$" + '{:,}'.format(mnSales + ohSales + txSales + scSales)),
                ]),
            ]),
        ]),  
           
        
            

        html.Div([
            html.Div([
                html.H5(html.P(["QUOTATIONS", html.Br(),""])),
                html.P(" "),
                html.P("Minnesota: "), 
                html.P("Daily: " + str(mnQuoteCount), className = 'indent'),
                html.P("Monthly: " + str(mnMonthlyQuoteCount), className= 'indent'),
                html.P("Ohio: "),
                html.P("Daily: " + str(ohQuoteCount), className ='indent'),
                html.P("Monthly: " + str(ohMonthlyQuoteCount), className= 'indent'),
                html.P("Texas: "),
                html.P("Daily: " + str(txQuoteCount), className ='indent'),
                html.P("Monthly: " + str(txMonthlyQuoteCount), className= 'indent'),
                html.P("South Carolina: "),
                html.P("Daily: " + str(scQuoteCount), className = 'indent'),
                html.P("Monthly: " + str(scMonthlyQuoteCount), className= 'indent'),
                html.H6("Total: " + str(mnQuoteCount + ohQuoteCount + txQuoteCount + scQuoteCount)),
                html.H6("Total Monthly: " + str(mnMonthlyQuoteCount + ohMonthlyQuoteCount + txMonthlyQuoteCount + scMonthlyQuoteCount))
            ], className = 'two columns'),

             html.Div([
                html.H5(html.P(["ORDERS:", html.Br(),"New Dies"])),
                html.P("Minnesota: "),
                html.P("Daily: " + str(mnNewDies), className = 'indent'),
                html.P("Monthly: " + str(mnMonthlyNewDies),className = 'indent'),
                html.P("Ohio: "),
                html.P("Daily: " + str(ohNewDies), className = 'indent'),
                html.P("Monthly: " + str(ohMonthlyNewDies),className = 'indent'),
                html.P("Texas: "),
                html.P("Daily: " + str(txNewDies), className = 'indent'),
                html.P("Monthly: " + str(txMonthlyNewDies),className = 'indent'),
                html.P("South Carolina: "),
                html.P("Daily: " + str(scNewDies), className = 'indent'),
                html.P("Monthly: " + str(scMonthlyNewDies),className = 'indent'),
                html.H6("Total: " + str(mnNewDies + ohNewDies + txNewDies + scNewDies)),
                html.H6("Total Monthly: " + str(mnMonthlyNewDies + ohMonthlyNewDies + txMonthlyNewDies + scMonthlyNewDies))
            ], className = 'three columns'),

            

            html.Div([
                html.H5(html.P(["ORDERS:", html.Br(),"Reorders"])),
                html.P("Minnesota: "),
                html.P("Daily: " + str(mnReOrders), className = 'indent'),
                html.P("Monthly " + str(mnMonthlyReOrders), className = 'indent'),
                html.P("Ohio: "),
                html.P("Daily: " + str(ohReOrders), className = 'indent'),
                html.P("Monthly: " + str(ohMonthlyReOrders), className = 'indent'),
                html.P("Texas: "),
                html.P("Daily: " + str(txReOrders), className = 'indent'),
                html.P("Monthly: " + str(txMonthlyReOrders), className = 'indent'),
                html.P("South Carolina: "),
                html.P("Daily: " + str(scReOrders), className = 'indent'),
                html.P("Monthly: " + str(scMonthlyReOrders), className = 'indent'),
                html.H6("Total: " + str(mnReOrders + ohReOrders + txReOrders + scReOrders)),
                html.H6("Total Monthly: " + str(mnMonthlyReOrders + ohMonthlyReOrders + txMonthlyReOrders + scMonthlyReOrders))
            ], className = 'three columns'),

            html.Div([
                    html.H5(html.P("**FORECAST**")),
                    html.P(" "),
                    html.P("Minnesota: $"+ '{:,}'.format(mNForecast)),
                    html.P("Ohio: $"+ '{:,}'.format(oHForecast)),
                    html.P("Texas: $"+ '{:,}'.format(tXForecast)),
                    html.P("South Carolina: $"+ '{:,}'.format(sCForecast)),
                    html.H6("Total: $"+ '{:,}'.format(totalForecast))
            ], className = 'two columns'),

            html.Div([
                    html.H5(html.P("Sales Backlog")),
                    html.P(" "),
                    html.P("Minnesota: $" + '{:,}'.format(mnSales)),
                    html.P("Ohio: $" + '{:,}'.format(ohSales)),
                    html.P("Texas: $" + '{:,}'.format(txSales)),
                    html.P("South Carolina: $" + '{:,}'.format(scSales)),
                    html.H6("Total: $" + '{:,}'.format(mnSales + ohSales + txSales + scSales)),
            ], className = 'two columns'),

             
        ], className = 'row'),

                

        html.Div([
            
            html.Div([
                html.H5(html.P(["RELEASES:", html.Br(),"Tool Labor"])),
                html.P("Minnesota:"),
                html.P("Daily: $"+ '{:,}'.format(mnRelToolLabour),className= 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(mnWeeklyRelToolLabour), className='indent'),
                html.P("Ohio: "),
                html.P("Daily: $"+ '{:,}'.format(ohRelToolLabour), className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(ohWeeklyRelToolLabour),className = 'indent'),
                html.P("Texas: "),
                html.P("Daily: $" + '{:,}'.format(txRelToolLabour),className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(txWeeklyRelToolLabour),className = 'indent'),
                html.P("South Carolina:"),
                html.P("Daily: $" + '{:,}'.format(scRelToolLabour),className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(scWeeklyRelToolLabour),className = 'indent'),
                html.H6("Total: $"+ '{:,}'.format(mnRelToolLabour + ohRelToolLabour + txRelToolLabour + scRelToolLabour)),
                html.H6("Total Monthly: $"+ '{:,}'.format(mnWeeklyRelToolLabour + ohWeeklyRelToolLabour + txWeeklyRelToolLabour + scWeeklyRelToolLabour))
            ], className = 'three columns'),

            html.Div([
                   html.H5(html.P(["RELEASES:", html.Br(),"Press Labor"])),
                html.P("Minnesota:"),
                html.P("Daily: $"+ '{:,}'.format(mnRelPressLabour),className= 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(mnWeeklyRelPressLabour), className='indent'),
                html.P("Ohio: "),
                html.P("Daily: $"+ '{:,}'.format(ohRelPressLabour), className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(ohWeeklyRelPressLabour),className = 'indent'),
                html.P("Texas: "),
                html.P("Daily: $" + '{:,}'.format(txRelPressLabour),className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(txWeeklyRelPressLabour),className = 'indent'),
                html.P("South Carolina:"),
                html.P("Daily: $" + '{:,}'.format(scRelPressLabour),className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(scWeeklyRelPressLabour),className = 'indent'),
                html.H6("Total: $"+ '{:,}'.format(mnRelPressLabour + ohRelPressLabour + txRelPressLabour + scRelPressLabour)),
                html.H6("Total Monthly: $"+ '{:,}'.format(mnWeeklyRelPressLabour + ohWeeklyRelPressLabour + txWeeklyRelPressLabour + scWeeklyRelPressLabour))
            ], className = 'two columns'),

            html.Div([
                html.H5(html.P(["SHIPMENTS:", html.Br(),"Tool Labor"])),
                html.P("Minnesota: "),
                html.P("Daily: $ " + '{:,}'.format(mn_ship_tool_labor), className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(mn_ship_tool_labor_week), className = 'indent'),
                html.P("Ohio: "),
                html.P("Daily: $ " + '{:,}'.format(oh_ship_tool_labor), className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(oh_ship_tool_labor_week), className = 'indent'),
                html.P("Texas: "),
                html.P("Daily: $ " + '{:,}'.format(tx_ship_tool_labor), className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(tx_ship_tool_labor_week), className = 'indent'),
                html.P("South Carolina: "),
                html.P("Daily: $ " + '{:,}'.format(sc_ship_tool_labor), className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(sc_ship_tool_labor_week), className = 'indent'),
                html.H6("Total: $"+ '{:,}'.format(mn_ship_tool_labor + oh_ship_tool_labor + tx_ship_tool_labor + sc_ship_tool_labor)),
                html.H6("Total Weekly: $"+ '{:,}'.format(mn_ship_tool_labor_week + oh_ship_tool_labor_week + tx_ship_tool_labor_week + sc_ship_tool_labor_week))
            ], className = 'four columns'),

            html.Div([
                html.H5(html.P(["SHIPMENTS:", html.Br(),"Press Labor"])),
                html.P("Minnesota: "),
                html.P("Daily: $ " + '{:,}'.format(mn_ship_press_labor), className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(mn_ship_press_labor_week), className = 'indent'),
                html.P("Ohio: "),
                html.P("Daily: $ " + '{:,}'.format(oh_ship_press_labor), className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(oh_ship_press_labor_week), className = 'indent'),
                html.P("Texas: "),
                html.P("Daily: $ " + '{:,}'.format(tx_ship_press_labor), className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(tx_ship_press_labor_week), className = 'indent'),
                html.P("South Carolina: "),
                html.P("Daily: $ " + '{:,}'.format(sc_ship_press_labor), className = 'indent'),
                html.P("Weekly: $"+ '{:,}'.format(sc_ship_press_labor_week), className = 'indent'),
                html.H6("Total: $"+ '{:,}'.format(mn_ship_press_labor + oh_ship_press_labor + tx_ship_press_labor + sc_ship_press_labor)),
                html.H6("Total Weekly: $"+ '{:,}'.format(mn_ship_press_labor_week + oh_ship_press_labor_week + tx_ship_press_labor_week + sc_ship_press_labor_week))
            ], className = 'three columns'),

            
        ],className = 'row'),

        html.Div([
            
            html.Div([
                dcc.Graph(
                        id='fifth',
                        figure={
                            'data': [
                                {'x': ['Minneapolis', 'Ohio', 'South Carolina', 'Texas'],
                                'y': [mnMonthlyTotalOrders,ohMonthlyTotalOrders,scMonthlyTotalOrders,txMonthlyTotalOrders],
                                'type': 'bar', 'name': 'Bookings'},

                                {'x': ['Minneapolis', 'Ohio', 'South Carolina', 'Texas'],
                                'y': [mNForecast, oHForecast, sCForecast, tXForecast],
                                'type': 'bar', 'name': 'Forecast'}
                            ],
                            'layout': {'title': 'Bookings Vs Forecast'}
                        }

                    ),
                ], className='four columns long'),

            html.Div([
                    html.H5(html.P(["Total Orders Booked", html.Br(),""])),
                    html.P("Minnesota: "),
                    html.P("Daily: $"+ '{:,}'.format(round(mnTotalOrders)), className = 'indent'),
                    html.P((["Monthly: $"+ '{:,}'.format(round(mnMonthlyTotalOrders)) + ' --- {:,}'.format(round((mnMonthlyTotalOrders/mNForecast)*100)) + "% of forecast"]), className = 'indent'),
                    html.P("Ohio:"), 
                    html.P("Daily: $"+ '{:,}'.format(round(ohTotalOrders)), className = 'indent'),
                    html.P((["Monthly: $"+ '{:,}'.format(round(ohMonthlyTotalOrders)) + ' --- {:,}'.format(round((ohMonthlyTotalOrders/oHForecast)*100)) + "% of forecast"]), className = 'indent'),
                    html.P("Texas:"),
                    html.P("Daily: $"+ '{:,}'.format(round(txTotalOrders)), className = 'indent'),
                    html.P((["Monthly: $"+ '{:,}'.format(round(txMonthlyTotalOrders)) + ' --- {:,}'.format(round((txMonthlyTotalOrders/tXForecast)*100)) + "% of forecast"]), className = 'indent'),
                    html.P("South Carolina:"),
                    html.P("Daily: $"+ '{:,}'.format(round(scTotalOrders)), className = 'indent'),
                    html.P((["Month: $"+ '{:,}'.format(round(scMonthlyTotalOrders)) + ' --- {:,}'.format(round((scMonthlyTotalOrders/sCForecast)*100)) + "% of forecast"]), className = 'indent'),
                    html.H6("Total: $"+ '{:,}'.format(round(mnTotalOrders + ohTotalOrders + txTotalOrders + scTotalOrders))),
                    html.H6(["Total: $"+ '{:,}'.format(round(mnMonthlyTotalOrders + ohMonthlyTotalOrders + txMonthlyTotalOrders + scMonthlyTotalOrders)), html.Br(), 
                        '{:,}'.format(round(((mnMonthlyTotalOrders + ohMonthlyTotalOrders + txMonthlyTotalOrders + scMonthlyTotalOrders)/totalForecast)*100)) + "% of forecast"]),   
                ], className = 'two columns long'),
        

            html.Div([
                dcc.Graph(
            id='sixth',
            figure={
                'data': [
                    {'x': ['Minneapolis', 'Ohio', 'South Carolina', 'Texas'],
                     'y': [mn_billings_month,oh_billings_month,sc_billings_month,tx_billings_month],
                     'type': 'bar', 'name': 'Billing'},

                    {'x': ['Minneapolis', 'Ohio', 'South Carolina', 'Texas'],
                     'y': [mNForecast, oHForecast, sCForecast, tXForecast],
                     'type': 'bar', 'name': 'Forecast'}
                ],
                'layout': {'title': 'Billing Stats Vs Forecast'}
               
            }
        )

            ], className='four columns long'), 



            html.Div([
                html.H5(html.P(["Billings (Sales)", html.Br(),""])),
                html.P("Minnesota: "),
                html.P("Daily: $" + '{:,}'.format(mn_billings), className = 'indent'),
                html.P((["Monthly: $"+ '{:,}'.format(mn_billings_month) +' --- {:,}'.format(round((mn_billings_month/mNForecast)*100)) + "% of forecast"]), className = 'indent'),
                html.P("Ohio: "),
                html.P("Daily: $" + '{:,}'.format(oh_billings), className = 'indent'),
                html.P((["Monthly: $"+ '{:,}'.format(oh_billings_month) + ' --- {:,}'.format(round((oh_billings_month/oHForecast)*100)) + "% of forecast"]), className = 'indent'),
                html.P("Texas: "),
                html.P("Daily: $" + '{:,}'.format(tx_billings), className = 'indent'),
                html.P((["Monthly: $"+ '{:,}'.format(tx_billings_month) + ' --- {:,}'.format(round((tx_billings_month/tXForecast)*100)) + "% of forecast"]), className = 'indent'),
                html.P("South Carolina: "),
                html.P("Daily: $" + '{:,}'.format(sc_billings), className = 'indent'),
                html.P((["Month: $"+ '{:,}'.format(sc_billings_month) + ' --- {:,}'.format(round((sc_billings_month/sCForecast)*100)) + "% of forecast"]), className = 'indent'),
                html.H6("Total: $"+ '{:,}'.format(mn_billings + oh_billings + tx_billings + sc_billings)),
                html.H6(["Total: $"+ '{:,}'.format(mn_billings_month + oh_billings_month + tx_billings_month + sc_billings_month), html.Br(), 
                    '{:,}'.format(round(((mn_billings_month + oh_billings_month + tx_billings_month + sc_billings_month)/totalForecast)*100)) + "% of forecast"]),  
            ], className = 'two columns long'),

           

        ], className = 'row'),

       

    ], className = 'twelve columns'),


    ])
   
    # Start the Flask app server 
    if __name__ == '__main__':
   
       # Set up flask app if not in dev mode
       if devmode == True:
          app.run_server(host='0.0.0.0') #Uncomment this line to run with Flash dev server
          #app.run_server(host='10.251.20.20') #Uncomment this to run Flask dev server on just IBM i address
       else:
          app.run_server(debug=True) #Uncomment this line to run with Gunicorn


def quoteCount(schema, start_date, end_date):
    #counting daily quotes
    sql = '''select count(qctqtn) 
             from {0}.qtcnt00p
             where qctdat between {1} and {2}
                AND qctsta ='N' '''.format(schema,
                                    start_date.strftime('%Y%m%d'),
                                    end_date.strftime('%Y%m%d'))

    logging.info('quoteCount schema - %s', schema)
    logging.info('quoteCount start_date - %s', start_date)
    logging.info('quoteCount end_date - %s', end_date)
    logging.info('quotecount sql - %s', sql)

    quote_count = cursor.execute(sql).fetchval()
    
    return quote_count


def reOrders(schema, start_date, end_date):
    #counting daily re-orders
    sql = ('''select count(distinct t1.rlfjob) "Rework"
              from {0}.relfile t1
                exception join
                (
                    select rlfjob 
                    from {0}.relfile
                    where rlfdta < {1}
                ) t2 on t1.rlfjob = t2.rlfjob
              where rlfdta between {1} and {2}
                and t1.rlfjob like '%K%' '''.format(schema,
                                                    start_date.strftime('%y%m%d'),
                                                    end_date.strftime('%y%m%d')))
    
    
    logging.info('reOrders schema - %s',schema)
    logging.info('reOrders sql - %s',sql)
    logging.info('reOrders start_date - %s',start_date.strftime('%y%m%d'))
    logging.info('reOrders end_date - %s',end_date.strftime('%y%m%d'))

    reorder_count = cursor.execute(sql).fetchval()
    
    return reorder_count


def newDies(schema, start_date, end_date):
    sql = '''select count(distinct t1.rlfjob) "New Dies"
             from {0}.relfile t1 exception join
                (select rlfjob 
                 from {0}.relfile 
                 where rlfdta < {1}
             )t2 on t1.rlfjob = t2.rlfjob
             where rlfdta between {1} and {2}
                and t1.rlfjob like '%T%' '''.format(schema,
                                                    start_date.strftime('%y%m%d'),
                                                    end_date.strftime('%y%m%d'))
    
    
    logging.info('newDies schema - %s',schema)
    logging.info('newDies sql - %s',sql)
    logging.info('newDies start_date - %s',start_date.strftime('%y%m%d'))
    logging.info('newDies end_date - %s',end_date.strftime('%y%m%d'))

    new_dies_count = cursor.execute(sql).fetchval()
    
    return new_dies_count


def get_arinv_data(data_element, plant, start_date, end_date):
    elements = {'ship_tool_labor': 'sum(arengsal)',
                'ship_press_labor': 'sum(arlabsal)',
                'billings': 'sum(arbilamt)'}
    
    sql = '''select {0} 
             from drdata.arinv
             where ARINVDAT between '{1}' and '{2}'
                AND ARJOB NOT LIKE '%J%' 
                and arjob not like '%R%'
                and arplt = {3} '''.format(elements.get(data_element), start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d'), plant)
    if data_element != 'billings':
        sql = sql + 'and arcrdmem is null'
    
    logging.info('get_arinv_data start_date - %s',start_date)
    logging.info('get_arinv_data end_date - %s',end_date)
    logging.info('get_arinv_data sql - %s',sql)

    arinv_data = cursor.execute(sql).fetchval()

    if arinv_data:
        return round(arinv_data)
    else:
        return 0


def _parse_db_data(db_data):
    columns = [column[0] for column in db_data.description]
    data = []

    for row in db_data.fetchall():
        data.append(dict(zip(columns, row)))
    
    return data


def totalOrdersBooked(schema,start_date,end_date):
    sql = '''select sum(s1.orders_booked)
            from (
                select rlfjob job, (rlfqty * rlfprc) + rlfeng orders_booked
                from {0}.relfile
                where rlfdta = {1}
                    and {0}.jobtype(rlfjob) in ('T','K')
                union
                select t1.rlfjob job, -1 * (rlfqty * rlfprc) + rlfeng orders_booked
                from {0}.relfile t1 inner join
                    (
                        select rlfjob maxjob, max(rlfdta) maxdate
                        from {0}.relfile
                        where rlfdta < {1}
                            and rlfjob in (
                                select rlfjob
                                from {0}.relfile
                                where rlfdta = {1} )
                        group by rlfjob
                    ) on t1.rlfjob = maxjob and t1.rlfdta = maxdate
                where {0}.jobtype(t1.rlfjob) in ('T','K')
            ) s1 '''.format(schema,
                                start_date.strftime('%y%m%d'),
                                end_date.strftime('%y%m%d'))

    logging.info('totalOrdersBooked schema - %s',schema)
    logging.info('totalOrdersBooked start_date - %s',start_date)
    logging.info('totalOrdersBooked end_date - %s',end_date)
    logging.info('totalOrdersBooked sql - %s',sql)

    total_orders_booked = cursor.execute(sql).fetchval()
    
    if total_orders_booked:
        return round(total_orders_booked)
    else:
        return 0


def relPressLabour(schema,start_date,end_date):
    sql = '''select sum(s1.press_labor) release_press_labor
            from (
                select distinct t1.rlfjob job, (rlfqty * rlflab) press_labor
                    from {0}.relfile t1 exception join
                    (
                        select rlfjob 
                        from {0}.relfile 
                        where rlfdta < {1}
                            and rlflab <> 0
                        ) t2 on t1.rlfjob = t2.rlfjob
                    where rlfdta between {1} and {2}
                        and {0}.jobtype(t1.rlfjob) in ('T','K')
                    group by t1.rlfjob, rlfqty, rlflab
                    order by t1.rlfjob
            ) s1'''.format(schema,
                            start_date.strftime('%y%m%d'),
                            end_date.strftime('%y%m%d'))
    
    logging.info('relPressLabour schema - %s',schema)
    logging.info('relPressLabour start_date - %s',start_date)
    logging.info('relPressLabour end_date - %s',end_date)
    logging.info('relPressLabour sql - %s',sql)

    pLabour = cursor.execute(sql).fetchval()
    
    if pLabour:
        return round(pLabour)
    else:
        return 0


def relToolLabour(schema,start_date,end_date):
    sql = '''select sum(rlfeng) "RELEASES - TOOL LABOR"
             from {0}.relfile t1 exception join
             (
                 select rlfjob 
                 from {0}.relfile
                 where rlfdta < {1}
                    and rlflab <> 0
             ) t2 on t1.rlfjob = t2.rlfjob
             where rlfdta between {1} and {2}'''.format(schema,
                                                        start_date.strftime('%y%m%d'),
                                                        end_date.strftime('%y%m%d'))
    
    logging.info('relToolLabour schema - %s',schema)
    logging.info('relToolLabour start_date - %s',start_date)
    logging.info('relToolLabour end_date - %s',end_date)
    logging.info('relToolLabour sql - %s',sql)

    rel_tool_labor = cursor.execute(sql).fetchval()
    
    if rel_tool_labor:
        return round(rel_tool_labor)
    else:
        return 0


def salesBackLog(tableName,salesName):
    sql = ''' select distinct sum(apri * case when jssqty not like '' then jssqty else jquan - jqtysh end) as sale
              from {0} inner join qs36f.jobmast0 on ajobn = jjob and aplt = jplt
                left outer join {1} on ajobn = jsjob#
              where astat <> 'B'
                and apri <> 0
                and jquan - jqtysh > 0
              group by aplt'''.format(tableName,salesName)
    cursor.execute(sql)
    sales=cursor.fetchall()
    
    logging.info('salesBackLog tableName - %s',tableName)
    logging.info('salesBackLog salesName - %s',salesName)
    logging.info('salesBackLog sql - %s',sql)
    
    sl=""
    for a in str(sales):

        if a.isalpha() or a=="[" or a=="(" or a=="," or a==")" or a=="]" or a=="'":
            continue
        else:
            sl += a
    if sl!="":
        return round(float(sl))

    else:
        return 0
    

mainProgram()