import pandas as pd
from openpyxl import Workbook
from openpyxl.utils.dataframe import dataframe_to_rows

def xlsx_write(data_frame,workbook_name):
    workbook = Workbook()
    sheet = workbook.active

    for row in dataframe_to_rows(data_frame, index=True, header=True):
        sheet.append(row)

    workbook.save(workbook_name)