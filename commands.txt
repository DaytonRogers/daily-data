*nix
python3 -m venv venv
source venv/bin/activate
export FLASK_APP=flaskr
export FLASK_ENV=development
flask run

Windows
python -m venv venv
venv\Scripts\activate
$env:FLASK_APP = "flaskr"
$env:FLASK_ENV = "development"
flask run