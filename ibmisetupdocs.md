# Daily Data Python Web App Set Up on IBMi

## Gunicorn Related Files
Created startup and shutdown bash scripts
```
/gunicorn
/gunicorn.strgunicorn.sh
/gunicorn.endgunicorn.sh
/gunicorn.strnginx.sh
/gunicorn.endnginx.sh
```

## NGINX Config file
Created an NGINX config to proxy access to Gunicorn on address 127.0.0.1:9112
```
/qopensys/etc/nginx/nginx.conf
```

## Daily Data Python App Location
This is the Python app folder cloned from github
```
/pythonapps/daily-data/daily_data/
```

## JOB Description PASEJOBD in QGPL
Copied programmer JOBD for PASE jobs. Don’t want to gen spool file joblogs for threads.

***PASEJOBD in library QGPL*** – Copy of PGMRJOBD but will not create a bunch of thread joblogs. Set to 4/0/*NOLIST

## Installed QSHONI library 
The QSHEXEC command is used to run QSH commands in batch.

## IBM i Gunicorn Startup Commands

***OPNPATH*** – This command sets IBMi job path to /QOpenSys/pkgs/bin so the job can find open system commands. 

***CALL RSCHOEN/SBMGUNICRN*** – Calls STRGUNICRN in batch mode. Submits CALL STRGUNICRN to QSYSNOMAX jobq so it doesn’t clog anything up.  

***CALL RSCHOEN/STRGUNICRN*** – Start NGINX server by calling /gunicorn/strnginx.sh and start Gunicorn server by calling /gunicorn/strgunicorn.sh. **Note: This command should be submitted to run by calling SBMGUNICRN.

***CALL RSCHOEN/ENDGUNICRN*** – End NGINX server by calling /gunicorn/endnginx.sh and end Gunicorn server by calling /gunicorn/endgunicorn.sh

## TCP Ports used
```
NGINX: 0.0.0.0:9111
Gunicorn: 127.0.0.1:9112
```

## URL to access dayRog.py dashboard app:
```
http://10.251.20.20:9111
```

## Notable Changes (2020-6-18)
There is now a devmode variable at top of script. Controls a couple small Flask nuance settings for Gunicorn vs Development mode
```
Set = True if running Flask dev server via: python3 dayRog.py
Set = False if running via Gunicorn
```
GIT changes to production
```
cd /pythonapps/daily-data
git pull
```