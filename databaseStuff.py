import xlrd
import pyodbc as pbc


def gettingDatabaseName():  # a function to get the database name from the user
    try:
        print("Kindly type in your database name")
        userInput = input().strip().replace(" ", "")

        while userInput == "":
            print("Kindly type in your database name")
            userInput = input().strip().replace(" ", "")
        return userInput
    except Exception:
        pass


def gettingServerName():  # a function to get the server name from the user
    try:
        print("Kindly type in your database server name")
        userInput = input().strip()
        return userInput

    except Exception:
        pass


def gettingSheetName():  # a function to get the excel file sheet name from the user
    try:
        print("Kindly specify the name of the sheet that contains data in your excel file")
        userInput = input().strip()
        return userInput
    except Exception:
        pass


def gettingTableName():  # a function to give the table getting created in the database a name
    try:
        print("Kindly give a name for the table")
        userInput = input().strip().replace(" ", "")
        while userInput == "":
            print("Kindly give a name for the table")
            userInput = input().strip().replace(" ", "")

        return userInput

    except Exception:
        pass


def gettingExcelFileName():  # a function to get the excel file name from the user
    print("Kindly type in the name of the excel file ")
    userInput = input()
    eFile = userInput + ".xlsx"  # after the file name is retrieved, the excel extension .xlsx  is added to the excel file name
    return eFile


def mainProgram(excel, sheet, server, database,table):  # main function accepting all the inputs of the user as parameters
    book = xlrd.open_workbook(excel)  # opening the excel file
    sheet = book.sheet_by_name(sheet)  # putting selection on the specified sheet in the excel file

    cout = 0  # count is set to zero here and will be use later on to help show the number of data that was pulled into your database

    # Establishing SQL connection
    conn = pbc.connect('DRIVER={SQL Server};SERVER=' + server + ';DATABASE=' + database + ';Trusted_Connection=yes')

    # Get the cursor, which is used to traverse the database, line by line
    cursor = conn.cursor()

    b = " "
    columns = ""
    columnNames = []  # a list to contain the column names in the excel sheet
    boo = []
    noOfColumns = 0  # this counter is used to track the number of columns headings that are not empty
    for c in range(0,
                   sheet.ncols):  # looping through the columns of the specified excel sheet to get the headings of the columns
        if (sheet.cell(0, c).value != ""):
            columnNames.append(sheet.cell(0, c).value)
            columns = columnNames
            noOfColumns += 1

    q = ""
    k = []
    for r in columns:  # this loop puts the entire column names that is held in columns into k without spaces or . or hyphens
        q = r.replace(" ", "")
        c = q.replace(".", "")
        f = c.replace("-", "")
        k.append(f)
    m = " "
    for u in range(
            noOfColumns):  # this loop goes through the names in list k takes away any commas anywhere and adds varchar 255 to the names and finally stores them in  variable m
        if (u == 0):
            m += str(k[u].replace(",", "") + " varchar(255)")
        else:
            m += str(',' + k[u].replace(",", "") + " varchar(255)")

    # query creating the  table
    query1 = """                                            
                            CREATE TABLE {0}.dbo.{1} 
                            (              
                                {2}
                            )""".format(database, table, m)

    a = []
    e = " "
    p = " "
    for count in range(noOfColumns):
        a.append("?")
        p = str(a)

    for c in range(noOfColumns):
        if (c == 0):
            b += str(a[c])

        # elif (c==noOfColumns-1):
        #     b += str(',' + a[c]+',')
        #     e += str(','+ s[c]+',')
        else:
            b += str(',' + a[c])
            # e += str(','+s[c]).replace(" ","")
    e = m.replace("varchar(255)", "")
    query = """INSERT INTO {0}.dbo.{1} 
                (
                {2}    

                )
                values ({3})""".format(database, table, e, b)

    # execute create table
    try:
        cursor.execute(query1)
        conn.commit()
    except pbc.ProgrammingError:
        pass

    la = ""
    up = {}
    # Create a For loop to iterate through each row in the XLS file, starting at row 2 to skip the headers
    for r in range(1, sheet.nrows):
        for h in range(noOfColumns):
            up = {k[h]: sheet.cell(r, h).value}
            k[h] = str(up[k[h]])

        la = k
        values = la
        cout += 1
        # execute query
        cursor.execute(query, values)

    # Close the cursor
    cursor.close()

    # Commit the transaction
    conn.commit()

    # Close the database connection
    conn.close()

    # Print results
    print("")
    print("All Done! Bye, for now.")
    print("")

    print("I just imported ", str(cout), " records to SQL!")


dName = gettingDatabaseName()
eName = gettingExcelFileName()
sName = gettingServerName()
shName = gettingSheetName()
tName = gettingTableName()
mainProgram(eName, shName, sName, dName, tName)

