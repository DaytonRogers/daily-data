import pandas
import ibm_db_dbi as db2
import dash
import dash_html_components as html
import dash_core_components as dcc

# making connection to database
conn = db2.connect()

# varible to implement queries
cursor = conn.cursor()

# variable to be used to get yesterday's date
cursor.execute('''values dayofweek(curdate());''')  # this statement gets the current date
dayNum = str(
    cursor.fetchall())  # then its stored in the variable dayNum but it comes in a format that includes brackets,square brackets,commas etc
dayOfWeek = ""
# looping through dayNum and getting rid of all irrelevant characters
for c in dayNum:
    if c.isalpha() or c == "[" or c == "(" or c == "," or c == ")" or c == "]" or c == ".":
        continue
    else:
        dayOfWeek += c

# here dayton operates from mon-friday and i would say their data for the present data reflects that of the previous day
day = ""
if dayOfWeek == "1":  # just in case its a dayOfWeek==1 that is = sunday, yet they need to look at something due to emmergency then the subtraction will be minus 2 days in order to get the results on friday
    day = "2"
elif dayOfWeek == "2":  # if it's a monday then minus 3days else all is minus 1 day
    day = "3"
else:
    day = "1"

wkRelSubtractionDate = ""

if dayOfWeek == "2":  # monday
    cursor.execute('''select varchar_format(CURRENT TIMESTAMP - 0 DAYS, 'YYMMDD') FROM SYSIBM.SYSDUMMY1;''')
    wk = cursor.fetchall()
    for c in str(wk):
        if c.isalpha() or c == "[" or c == "(" or c == "," or c == ")" or c == "]" or c == "." or c == " " or c == "'":
            continue
        else:
            wkRelSubtractionDate += c

elif dayOfWeek == "3":  # tuesday
    cursor.execute('''select varchar_format(CURRENT TIMESTAMP - 1 DAYS, 'YYMMDD') FROM SYSIBM.SYSDUMMY1;''')
    wk = cursor.fetchall()
    for c in str(wk):
        if c.isalpha() or c == "[" or c == "(" or c == "," or c == ")" or c == "]" or c == "." or c == " " or c == "'":
            continue
        else:
            wkRelSubtractionDate += c


elif dayOfWeek == "4":  # wednesday
    cursor.execute('''select varchar_format(CURRENT TIMESTAMP - 2 DAYS, 'YYMMDD') FROM SYSIBM.SYSDUMMY1;''')
    wk = cursor.fetchall()
    for c in str(wk):
        if c.isalpha() or c == "[" or c == "(" or c == "," or c == ")" or c == "]" or c == "." or c == " " or c == "'":
            continue
        else:
            wkRelSubtractionDate += c

elif dayOfWeek == "5":  # thursday
    cursor.execute('''select varchar_format(CURRENT TIMESTAMP - 3 DAYS, 'YYMMDD') FROM SYSIBM.SYSDUMMY1;''')
    wk = cursor.fetchall()
    for c in str(wk):
        if c.isalpha() or c == "[" or c == "(" or c == "," or c == ")" or c == "]" or c == "." or c == " " or c == "'":
            continue
        else:
            wkRelSubtractionDate += c

elif dayOfWeek == "6":  # friday
    cursor.execute('''select varchar_format(CURRENT TIMESTAMP - 4 DAYS, 'YYMMDD') FROM SYSIBM.SYSDUMMY1;''')
    wk = cursor.fetchall()
    for c in str(wk):
        if c.isalpha() or c == "[" or c == "(" or c == "," or c == ")" or c == "]" or c == "." or c == " " or c == "'":
            continue
        else:
            wkRelSubtractionDate += c
else:
    cursor.execute('''select varchar_format(CURRENT TIMESTAMP , 'YYMMDD') FROM SYSIBM.SYSDUMMY1;''')
    wk = cursor.fetchall()
    for c in str(wk):
        if c.isalpha() or c == "[" or c == "(" or c == "," or c == ")" or c == "]" or c == "." or c == " " or c == "'":
            continue
        else:
            wkRelSubtractionDate += c

cursor.execute('''values month((curdate())-1 day);''')
pM = cursor.fetchall()
pMt = str(pM).replace(",", "")[
      2:4]  # getting today's date date only. Eg if today is the july 7 1990 is getting only the 7
pMth = pMt.replace(")", "")
print(pMth)

# getting the current month and storing it in cMonth
cursor.execute('''values month(curdate());''')
c = cursor.fetchall()
cMonth = ""
for a in str(c):
    if a.isalpha() or a == "[" or a == "(" or a == "," or a == ")" or a == "]" or c == ".":
        continue
    else:
        cMonth += a
print("You see me", cMonth)
# now we compare to find out if its the same month or it is a new month
if pMth == cMonth:
    # making variables to hold monthly counts
    cursor.execute('''values day(CURRENT TIMESTAMP) ;''')
    currentMonthday = cursor.fetchall()
    currentM = str(currentMonthday).replace(",", "")[
               2:4]  # getting today's date date only. Eg if today is the july 7 1990 is getting only the 7
    currentMonthFigure = currentM.replace(")", "")
    print("Hurrrrrrrrrrray", currentMonthFigure)

else:
    cursor.execute('''values day(CURRENT TIMESTAMP)-1 ;''')
    currentMonthday = cursor.fetchall()
    currentM = str(currentMonthday).replace(",", "")[2:4]
    currentMonthFigure = currentM.replace(")", "")

plant = [1, 3, 4, 6]


def mainProgram(plant, day, currentMonthFigure, wkRelSubtractionDate):
    for i in plant:
        if i == 1:
            relNames = "drdata.relfile"
            plantName = "drdata.qtcnt00p"
            mnQuoteCount = quoteCount(plantName, day)
            mnMonthlyQuoteCount = monthlyQuoteCount(plantName, currentMonthFigure)
            mnReOrders = reOrders(relNames, day)
            mnMonthlyReOrders = monthlyReOrders(relNames, currentMonthFigure)
            mnNewDies = newDies(relNames, day)
            mnMonthlyNewDies = monthlyNewDies(relNames, currentMonthFigure)
            mnTotalOrders = totalOrdersBooked(relNames, day)
            mnMonthlyTotalOrders = monthlyTotalOrdersBooked(relNames, currentMonthFigure)
            mnRelPressLabour = relPressLabour(relNames, day)
            mnWeeklyRelPressLabour = weeklyRelPressLabour(relNames, wkRelSubtractionDate)
            mnRelToolLabour = relToolLabour(relNames, day)
            mnWeeklyRelToolLabour = weeklyRelToolLabour(relNames, wkRelSubtractionDate)
            display(plantName, mnQuoteCount, mnMonthlyQuoteCount, mnReOrders, mnMonthlyReOrders,
                    mnNewDies, mnMonthlyNewDies, mnTotalOrders, mnMonthlyTotalOrders,
                    mnRelPressLabour, mnWeeklyRelPressLabour, mnRelToolLabour, mnWeeklyRelToolLabour)

        elif i == 3:
            relNames = "drohdata.relfile"
            plantName = "drohdata.qtcnt00p"
            ohQuoteCount = quoteCount(plantName, day)
            ohMonthlyQuoteCount = monthlyQuoteCount(plantName, currentMonthFigure)
            ohReOrders = reOrders(relNames, day)
            ohMonthlyReOrders = monthlyReOrders(relNames, currentMonthFigure)
            ohNewDies = newDies(relNames, day)
            ohMonthlyNewDies = monthlyNewDies(relNames, currentMonthFigure)
            ohTotalOrders = totalOrdersBooked(relNames, day)
            ohMonthlyTotalOrders = monthlyTotalOrdersBooked(relNames, currentMonthFigure)
            ohRelPressLabour = relPressLabour(relNames, day)
            ohWeeklyRelPressLabour = weeklyRelPressLabour(relNames, wkRelSubtractionDate)
            ohRelToolLabour = relToolLabour(relNames, day)
            ohWeeklyRelToolLabour = weeklyRelToolLabour(relNames, wkRelSubtractionDate)
            display(plantName, ohQuoteCount, ohMonthlyQuoteCount, ohReOrders, ohMonthlyReOrders,
                    ohNewDies, ohMonthlyNewDies, ohTotalOrders, ohMonthlyTotalOrders,
                    ohRelPressLabour, ohWeeklyRelPressLabour, ohRelToolLabour, ohWeeklyRelToolLabour)

        elif i == 4:
            relNames = "drtxdata.relfile"
            plantName = "drtxdata.qtcnt00p"
            txQuoteCount = quoteCount(plantName, day)
            txMonthlyQuoteCount = monthlyQuoteCount(plantName, currentMonthFigure)
            txReOrders = reOrders(relNames, day)
            txMonthlyReOrders = monthlyReOrders(relNames, currentMonthFigure)
            txNewDies = newDies(relNames, day)
            txMonthlyNewDies = monthlyNewDies(relNames, currentMonthFigure)
            txTotalOrders = totalOrdersBooked(relNames, day)
            txMonthlyTotalOrders = monthlyTotalOrdersBooked(relNames, currentMonthFigure)
            txRelPressLabour = relPressLabour(relNames, day)
            txWeeklyRelPressLabour = weeklyRelPressLabour(relNames, wkRelSubtractionDate)
            txRelToolLabour = relToolLabour(relNames, day)
            txWeeklyRelToolLabour = weeklyRelToolLabour(relNames, wkRelSubtractionDate)
            display(plantName, txQuoteCount, txMonthlyQuoteCount, txReOrders, txMonthlyReOrders,
                    txNewDies, txMonthlyNewDies, txTotalOrders, txMonthlyTotalOrders,
                    txRelPressLabour, txWeeklyRelPressLabour, txRelToolLabour, txWeeklyRelToolLabour)

        else:
            relNames = "drscdata.relfile"
            plantName = "drscdata.qtcnt00p"
            scQuoteCount = quoteCount(plantName, day)
            scMonthlyQuoteCount = monthlyQuoteCount(plantName, currentMonthFigure)
            scReOrders = reOrders(relNames, day)
            scMonthlyReOrders = monthlyReOrders(relNames, currentMonthFigure)
            scNewDies = newDies(relNames, day)
            scMonthlyNewDies = monthlyNewDies(relNames, currentMonthFigure)
            scTotalOrders = totalOrdersBooked(relNames, day)
            scMonthlyTotalOrders = monthlyTotalOrdersBooked(relNames, currentMonthFigure)
            scRelPressLabour = relPressLabour(relNames, day)
            scWeeklyRelPressLabour = weeklyRelPressLabour(relNames, wkRelSubtractionDate)
            scRelToolLabour = relToolLabour(relNames, day)
            scWeeklyRelToolLabour = weeklyRelToolLabour(relNames, wkRelSubtractionDate)
            display(plantName, scQuoteCount, scMonthlyQuoteCount, scReOrders, scMonthlyReOrders,
                    scNewDies, scMonthlyNewDies, scTotalOrders, scMonthlyTotalOrders,
                    scRelPressLabour, scWeeklyRelPressLabour, scRelToolLabour, scWeeklyRelToolLabour)

    arinv()

    conn.commit()

    cursor.close()

    conn.close()
    app = dash.Dash()
    app.layout = html.Div([
        html.H1("Dayton Rogers"),
        html.Div(children=[
            dcc.Graph(

                id='first',
                figure={
                    'data': [{'x': ['Minneapolis'], 'y': [6], 'type': 'bar',
                              'name': 'First Chart'}],
                    'layout': {'title': 'Quote Count Bar Chart'}
                }
            ),
        ]),
    ])

    app.run_server()


def quoteCount(plantName, day):
    count = 0
    cursor.execute('''select * from {0}
                     where (qctqdt = varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYYYMMDD'))
                    AND qctsta ='N';'''.format(plantName, day))
    quote = cursor.fetchall()

    for i in quote:
        count += 1
    return count


def monthlyQuoteCount(plantName, currentMonthFigure):
    monthlyQuotes = 0

    # counting monthly quote counts Minneapolis
    for i in range(1, int(currentMonthFigure)):

        cursor.execute('''select * from {0}
        where (qctqdt = varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYYYMMDD'))
        AND qctsta ='N';;'''.format(plantName, str(i)))
        quoteCountRecords = cursor.fetchall()

        for b in range(len(quoteCountRecords)):
            # print(str(quoteCountRecords[b]))
            monthlyQuotes += 1
    return monthlyQuotes


def reOrders(relNames, day):
    cursor.execute('''select count(distinct t1.rlfjob) "Rework"
    from {0} t1
    exception join
        (select rlfjob from {0} where rlfdta < varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD')) t2 on t1.rlfjob = t2.rlfjob
    where (rlfdta = varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD'))
        and t1.rlfjob like '%K%';'''.format(relNames, day))

    reData = cursor.fetchall()

    if (reData[0] != " "):
        reToString = str(reData[0]).replace(",", "")
        reSlice = reToString.replace("(", "")
        reFSlice = reSlice.replace(")", "")
        reReOrders = reFSlice
    else:
        reReOrders = 0
    return reReOrders


def monthlyReOrders(relNames, currentMonthFigure):
    noOfMonthlyReorders = 0
    # counting monthly re-orders for minneapolis
    for i in range(1, int(currentMonthFigure)):
        cursor.execute('''select count(distinct t1.rlfjob) "Rework"
                        from {0} t1
                        exception join
                            (select rlfjob from {0} where rlfdta < varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD')) t2 on t1.rlfjob = t2.rlfjob
                        where (rlfdta = varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD'))
                        and t1.rlfjob like '%K%';'''.format(relNames, str(i)))
        mnthReOrders = cursor.fetchall()

        for b in range(len(mnthReOrders)):

            if (mnthReOrders[0] != " "):
                mnReOrderToString = str(mnthReOrders[0]).replace(",", "")
                mnReOrderSlice = mnReOrderToString.replace("(", "")
                mnReOrderFSlice = mnReOrderSlice.replace(")", "")
                mnMonthlyReorders = mnReOrderFSlice
                noOfMonthlyReorders += int(mnMonthlyReorders)
            else:
                continue
    return noOfMonthlyReorders


def newDies(relNames, day):
    cursor.execute('''select count(distinct t1.rlfjob) "New Dies"
                    from {0} t1 exception join
                        (select rlfjob from {0} where rlfdta < varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD'))t2 on t1.rlfjob = t2.rlfjob
                    where (rlfdta = varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD'))
                        and t1.rlfjob like '%T%';'''.format(relNames, day))

    nDies = cursor.fetchall()

    if (nDies[0] != " "):
        newToString = str(nDies[0]).replace(",", "")
        newSlice = newToString.replace("(", "")
        newFSlice = newSlice.replace(")", "")
        newDie = newFSlice
    else:
        newDie = 0
    return newDie


def monthlyNewDies(relNames, currentMonthFigure):
    noOfMonthlyNewDies = 0
    # counting monthly re-orders for minneapolis
    for i in range(1, int(currentMonthFigure)):
        cursor.execute('''select count(distinct t1.rlfjob) "New Dies"
                        from {0} t1 exception join
                        (select rlfjob from {0} where rlfdta < varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD'))t2 on t1.rlfjob = t2.rlfjob
                        where (rlfdta = varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD'))
                        and t1.rlfjob like '%T%';'''.format(relNames, str(i)))
        mnthNewDies = cursor.fetchall()

        for b in range(len(mnthNewDies)):

            if (mnthNewDies[0] != " "):
                mnthNewDiesToString = str(mnthNewDies[0]).replace(",", "")
                mnthNewDiesSlice = mnthNewDiesToString.replace("(", "")
                mnthNewDiesFSlice = mnthNewDiesSlice.replace(")", "")
                mnthMonthlyNewDie = mnthNewDiesFSlice
                noOfMonthlyNewDies += int(mnthMonthlyNewDie)
            else:
                continue
    return noOfMonthlyNewDies


def arinv():
    # Minneapolis,Ohio,Texas and South Carolina Tool labour, Press Labour and Billed Amt for yesterday From the invoice file
    cursor.execute('''select arplt plant, sum(arengsal) engineering, sum(arlabsal) labor, sum(arbilamt) billed 
                    from drdata.arinv
                    where (ARINVDAT = varchar_format(CURRENT TIMESTAMP - {0} DAYS, 'YYYY-MM-DD'))
                    AND ARJOB NOT LIKE '%J%'
                    Group By ARPLT;'''.format(day))
    invShip = cursor.fetchall()

    for i in invShip:

        if i[0] == 1:
            toolLabour = invShip[0][1]
            mnToolLabour = toolLabour
            PressLabour = invShip[0][2]
            mnPressLabour = PressLabour
            billedAmt = invShip[0][3]
            mnBilledAmt = billedAmt

        elif i[0] == 3:
            toolLabour = invShip[1][1]
            ohToolLabour = toolLabour
            PressLabour = invShip[1][2]
            ohPressLabour = PressLabour
            billedAmt = invShip[1][3]
            ohBilledAmt = billedAmt

        elif i[0] == 4:
            toolLabour = invShip[2][1]
            txToolLabour = toolLabour
            PressLabour = invShip[2][2]
            txPressLabour = PressLabour
            billedAmt = invShip[2][3]
            txBilledAmt = billedAmt

        elif i[0] == 6:
            toolLabour = invShip[3][1]
            scToolLabour = toolLabour
            PressLabour = invShip[3][2]
            scPressLabour = PressLabour
            billedAmt = invShip[3][3]
            scBilledAmt = billedAmt
        else:
            pass

    print("Tool labour for Minneapolis is ", mnToolLabour)
    print("Tool labour for Ohio is ", ohToolLabour)
    print("Tool labour for Texas is ", txToolLabour)
    print("Tool labour for South Carolina is ", scToolLabour)
    print()
    print("Press labour for Minneapolis is ", mnPressLabour)
    print("Press labour for Ohio is ", ohPressLabour)
    print("Press labour for Texas is ", txPressLabour)
    print("Press labour for South is ", scPressLabour)
    print()
    print("Billed Amount for Minneapolis is ", mnBilledAmt)
    print("Billed Amount for Ohio is ", ohBilledAmt)
    print("Billed Amount for Texas is ", txBilledAmt)
    print("Billed Amount for South Carolina is ", scBilledAmt)


def totalOrdersBooked(relNames, day):
    # Total Orders Booked for previous day
    cursor.execute('''select sum(distinct rlfqty*rlfprc) + sum(rlfeng)
    from {0} t1 exception join
        (select rlfjob from {0} where rlfdta < varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD')) t2 on t1.rlfjob = t2.rlfjob
        where rlfdta between varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD') and varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD');'''.format(
        relNames, day))
    tOrders = cursor.fetchall()
    totalOrders = ""
    for a in str(tOrders):
        if a.isalpha() or a == "[" or a == "(" or a == "," or a == ")" or a == "]" or a == "'":
            continue
        else:
            totalOrders += a

    return round(float(totalOrders))


def monthlyTotalOrdersBooked(relNames, currentMonthFigure):
    for i in range(1):
        cursor.execute('''select sum(distinct rlfqty*rlfprc) + sum(rlfeng)
                        from {0} t1 exception join
                        (select rlfjob from {0} where rlfdta < varchar_format(CURRENT TIMESTAMP, 'YYMMDD'))
                        t2 on t1.rlfjob = t2.rlfjob 
                        where rlfdta between varchar_format(CURRENT TIMESTAMP - {1} days, 'YYMMDD')
                         and varchar_format(CURRENT TIMESTAMP, 'YYMMDD');'''.format(relNames,
                                                                                    str(int(currentMonthFigure) - 1)))
        wkOrders = cursor.fetchall()
        print(wkOrders)
        wkTotalOrders = ""
        mthTotalOrders = 0
        for a in str(wkOrders):
            if a.isalpha() or a == "[" or a == "(" or a == "," or a == ")" or a == "]" or a == "'":
                continue
            else:
                wkTotalOrders += a
        print(wkTotalOrders)

        # mthTotalOrders += int(wkTotalOrders)
        return mthTotalOrders


def relPressLabour(relNames, day):
    # press labour for previous day
    cursor.execute(''' select sum(distinct rlfqty*rlflab) "RELEASE - PRESS LABOR"
    from {0} t1 exception join
        (select rlfjob from {0} where rlfdta < varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD')) t2 on t1.rlfjob = t2.rlfjob
    where rlfdta between varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD') and
    varchar_format(CURRENT TIMESTAMP - {1} DAYS, 'YYMMDD');'''.format(relNames, day))
    rPL = cursor.fetchall()
    pLabour = ""
    for a in str(rPL):
        if a.isalpha() or a == "[" or a == "(" or a == "," or a == ")" or a == "]" or a == "'":
            continue
        else:
            pLabour += a
    return round(float(pLabour))


def weeklyRelPressLabour(relNames, wkRelSubtractionDate):
    # weekly press labour
    cursor.execute(''' select sum(distinct rlfqty*rlflab) "RELEASE - PRESS LABOR"
    from {0} t1 exception join
        (select rlfjob from {0} where rlfdta < varchar_format(CURRENT TIMESTAMP, 'YYMMDD')) t2 on t1.rlfjob = t2.rlfjob
    where rlfdta between {1} and varchar_format(CURRENT TIMESTAMP, 'YYMMDD');'''.format(relNames, wkRelSubtractionDate))
    wkRPL = cursor.fetchall()
    wkPLabour = ""
    for a in str(wkRPL):
        if a.isalpha() or a == "[" or a == "(" or a == "," or a == ")" or a == "]" or a == "'":
            continue
        else:
            wkPLabour += a
    return round(float(wkPLabour))


def relToolLabour(relNames, day):
    # tool labour for previous day
    cursor.execute('''select sum(rlfeng) "RELEASES - TOOL LABOR"
    from {0} t1 exception join
        (select rlfjob from {0} where rlfdta < varchar_format(CURRENT TIMESTAMP - {1} days, 'YYMMDD')) t2 on t1.rlfjob = t2.rlfjob
    where rlfdta between varchar_format(CURRENT TIMESTAMP - {1} days, 'YYMMDD') and varchar_format(CURRENT TIMESTAMP - {1} days,'YYMMDD');'''.format(
        relNames, day))
    rTL = cursor.fetchall()
    tLabour = ""
    for a in str(rTL):
        if a.isalpha() or a == "[" or a == "(" or a == "," or a == ")" or a == "]" or a == "'":
            continue
        else:
            tLabour += a
    return round(float(tLabour))


def weeklyRelToolLabour(relNames, wkRelSubtractionDate):
    # weekly tool labour
    cursor.execute('''select sum(rlfeng) "RELEASES - TOOL LABOR"
    from {0} t1 exception join
        (select rlfjob from {0} where rlfdta < varchar_format(CURRENT TIMESTAMP, 'YYMMDD')) t2 on t1.rlfjob = t2.rlfjob
    where rlfdta between {1} and varchar_format(CURRENT TIMESTAMP, 'YYMMDD' );'''.format(relNames,
                                                                                         wkRelSubtractionDate))
    wkRTL = cursor.fetchall()
    wkTLabour = ""
    for a in str(wkRTL):
        if a.isalpha() or a == "[" or a == "(" or a == "," or a == ")" or a == "]" or a == "'":
            continue
        else:
            wkTLabour += a
    return round(float(wkTLabour))


def display(name, qCount, mthQCount, rDies, mthRDies, nDies, mthNDies, totalOrders, monthlyTotalOrdersBooked,
            relPressLabour, weeklyRelPressLabour, relToolLabour, weeklyRelToolLabour):
    print("The quote count for {0} is {1}".format(name, qCount))
    print("The monthly quote count for {0} is {1}".format(name, mthQCount))
    print("The re-orders for {0} is {1}".format(name, rDies))
    print("The monthly re-orders for {0} is {1}".format(name, mthRDies))
    print("The new dies for {0} is {1}".format(name, nDies))
    print("The monthly new dies for {0} is {1}".format(name, mthNDies))
    print("Total Orders Booked for {0} is {1}".format(name, totalOrders))
    print("Monthly total Orders Booked for {0} is {1}".format(name, monthlyTotalOrdersBooked))
    print("Release Press Labour for {0} is {1}".format(name, relPressLabour))
    print("Weekly release Press Labour for {0} is {1}".format(name, weeklyRelPressLabour))
    print("Release Tool labour for {0} is {1}".format(name, relToolLabour))
    print("Weekly release Tool labour for {0} is {1}".format(name, weeklyRelToolLabour))
    print()


mainProgram(plant, day, currentMonthFigure, wkRelSubtractionDate)